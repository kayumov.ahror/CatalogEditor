﻿using CatalogEditor.Abstraction.Entities;
using CatalogEditor.Abstraction.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogEditor.Abstraction.Controllers
{
    public abstract class BaseController<T> : Controller
       where T : BaseEntity
    {
        /// <summary>
        /// Repository to apply CRUD operations with Entity
        /// </summary>
        public readonly IRepository<T> Repository;

        /// <summary>
        /// Base controller's constructor
        /// </summary>
        /// <param name="mapperFacade"></param>
        /// <param name="repository"></param>
        public BaseController(IRepository<T> repository)
        {
            Repository = repository;
        }

        /// <summary>
        /// Returns all entities
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<T> GetEntities()
        {
            return Repository.GetAll();
        }

        /// <summary>
        /// Returns specific entity by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public T GetEntity(Guid id)
        {
            return Repository.Get(id);
        }

        /// <summary>
        /// Creates a new entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]T entity)
        {
            try
            {
                if (entity == null)
                {
                    return BadRequest();
                }
                Repository.Create(entity);
                return await Repository.Commit() ? Ok() : (IActionResult)BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Updates existed entity
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Put([FromBody]T entity)
        {
            try
            {
                if (entity == null)
                {
                    return BadRequest();
                }
                Repository.Update(entity);
                return await Repository.Commit() ? Ok() : (IActionResult)BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Deletes existed entity by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                var entity = Repository.Get(id);
                if (entity == null)
                {
                    return NotFound();
                }
                Repository.Delete(entity);
                return await Repository.Commit() ? Ok() : (IActionResult)BadRequest();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}
