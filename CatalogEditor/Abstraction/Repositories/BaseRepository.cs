﻿using CatalogEditor.Abstraction.Entities;
using CatalogEditor.DataProvider;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogEditor.Abstraction.Repositories
{
    /// <summary>
    /// Base repository BL
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class RepositoryBase<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly DatabaseContext Context;

        public RepositoryBase(DatabaseContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Creates a new entity
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Create(T entity)
        {
            Context.Add(entity);
        }

        /// <summary>
        /// Updates entity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        public virtual void Update(T entity)
        {
            Context.Update(entity);
        }

        /// <summary>
        /// Deletes entity
        /// </summary>
        /// <param name="entity"></param>
        /// 
        public virtual void Delete(T entity)
        {
            Context.Remove(entity);
        }

        /// <summary>
        /// Gets entity by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T Get(Guid id)
        {
            return Context.Set<T>().Find(id);
        }

        /// <summary>
        /// Gets all entities
        /// </summary>
        /// <returns></returns>
        public virtual IQueryable<T> GetAll()
        {
            return Context.Set<T>();
        }

        /// <summary>
        /// Commits all changes
        /// </summary>
        /// <returns></returns>
        public virtual async Task<bool> Commit()
        {
            try
            {
                return await Context.SaveChangesAsync() != -1;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
