﻿using CatalogEditor.Abstraction.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogEditor.Abstraction.Repositories
{
    /// <summary>
    /// IRepository interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T>
        where T : BaseEntity
    {
        /// <summary>
        /// Creates a new entity
        /// </summary>
        /// <param name="entity"></param>
        void Create(T entity);

        /// <summary>
        /// Updates existing entity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        void Update(T entity);

        /// <summary>
        /// Deletes existing entity
        /// </summary>
        /// <param name="entity"></param>
        void Delete(T entity);

        /// <summary>
        /// Gets single entity by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T Get(Guid id);

        /// <summary>
        /// Gets all entities
        /// </summary>
        /// <returns></returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Saves changes
        /// </summary>
        /// <returns></returns>
        Task<bool> Commit();
    }
}
