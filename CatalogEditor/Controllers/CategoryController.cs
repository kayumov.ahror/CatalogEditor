﻿using CatalogEditor.Abstraction.Controllers;
using CatalogEditor.Abstraction.Repositories;
using CatalogEditor.Entities;
using Microsoft.AspNetCore.Mvc;

namespace CatalogEditor.Controllers
{
    /// <summary>
    /// Controller for Category
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CategoryController : BaseController<Category>
    {
        public CategoryController(IRepository<Category> repository) : base(repository)
        {
        }
    }
}
