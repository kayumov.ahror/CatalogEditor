﻿using CatalogEditor.Abstraction.Controllers;
using CatalogEditor.Abstraction.Repositories;
using CatalogEditor.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogEditor.Controllers
{
    /// <summary>
    /// Controller for Product
    /// </summary>
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ProductController : BaseController<Product>
    {
        public ProductController(IRepository<Product> repository) : base(repository)
        {
        }
    }
}
