﻿using CatalogEditor.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace CatalogEditor.DataProvider
{
    /// <summary>
    /// Database context
    /// </summary>
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }

        public override void Dispose()
        {
            base.Dispose();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}
