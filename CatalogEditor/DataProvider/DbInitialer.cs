﻿using CatalogEditor.Entities;
using System.Linq;

namespace CatalogEditor.DataProvider
{
    /// <summary>
    /// Database initializer
    /// </summary>
    public class DbInitialer
    {
        public static void Initialize(DatabaseContext context)
        {
            //Creates a database if it does not exists
            context.Database.EnsureCreated();
            if (!context.Categories.Any())
                CreateDemoData(context);
        }

        /// <summary>
        /// Creating test demo data
        /// </summary>
        private static void CreateDemoData(DatabaseContext context)
        {
            Category category = new Category() { Name = "Одежда, обувь, аксесуары" };
            Category verhn = new Category() { Name = "Верхняя" };
            Category jensOdejda = new Category() { Name = "Женская одежда" };
            Category plashy = new Category() { Name = "Плащи, тренчи" };
            for (int i = 0; i < 100; i++)
            {
                plashy.Products.Add(new Product() { Name = "Product" + i, Price = 60, Quantity = 10 });
            }
            verhn.Children.Add(plashy);
            jensOdejda.Children.Add(verhn);
            category.Children.Add(jensOdejda);
        }
    }
}
