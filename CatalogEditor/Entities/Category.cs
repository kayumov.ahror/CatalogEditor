﻿using CatalogEditor.Abstraction.Entities;
using System.Collections.Generic;

namespace CatalogEditor.Entities
{
    /// <summary>
    /// Category entity
    /// </summary>
    public class Category : BaseEntity
    {
        public ICollection<Category> Children { get; set; }
        public ICollection<Product> Products { get; set; }
    }
}
