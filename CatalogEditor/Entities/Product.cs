﻿using CatalogEditor.Abstraction.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogEditor.Entities
{
    /// <summary>
    /// Product entity
    /// </summary>
    public class Product : BaseEntity
    {
        public double Price { get; set; }
        public int Quantity { get; set; }
        public byte[] Picture { get; set; }
    }
}
