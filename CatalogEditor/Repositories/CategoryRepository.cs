﻿using CatalogEditor.Abstraction.Repositories;
using CatalogEditor.DataProvider;
using CatalogEditor.Entities;

namespace CatalogEditor.Repositories
{
    /// <summary>
    /// BL for Clothes repository
    /// </summary>
    public class CategoryRepository : RepositoryBase<Category>
    {
        public CategoryRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
