﻿using CatalogEditor.Abstraction.Repositories;
using CatalogEditor.DataProvider;
using CatalogEditor.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogEditor.Repositories
{
    public class ProductRepository : RepositoryBase<Product>
    {
        public ProductRepository(DatabaseContext context) : base(context)
        {
        }
    }
}
