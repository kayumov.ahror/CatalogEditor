using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Moq;
using System.Linq;
using System.Collections.Generic;
using FluentAssertions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CatalogEditor.Controllers;
using CatalogEditor.Abstraction.Repositories;
using CatalogEditor.Entities;

namespace CatalogEditorTests
{
    [TestClass]
    public class ControllerTests : BaseAutoMockedTest<ProductController>
    {
        [TestMethod]
        public void GetAll_Should_Return_All_Products()
        {
            //Creating products
            var products = Enumerable.Repeat(new Product() { Name = "TestProduct" }, 5);

            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.GetAll()).Returns(products.AsQueryable());

            //Testing
            var result = TestController.GetEntities();
            result.Should().Equal(products);
        }

        [TestMethod]
        public void GetEntity_Should_Return_Specific_Products()
        {
            //Creating product
            var product = new Product();

            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Get(product.Id)).Returns(product);

            //Testing
            var result = TestController.GetEntity(product.Id);
            result.Should().Be(product);
        }

        [TestMethod]
        public async Task Post_Valid_Product_Should_Return_OkResult()
        {
            //Creating valid product
            var product = new Product();

            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(true));

            //Testing
            var result = await TestController.Post(product);
            GetMock<IRepository<Product>>().Verify(x => x.Create(product), Times.Once);
            result.Should().BeOfType<OkResult>();
        }

        [TestMethod]
        public async Task Post_Invalid_Product_Should_Return_BadRequestResult()
        {
            //Creating valid product
            var product = new Product();

            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(false));

            //Testing
            var result = await TestController.Post(product);
            GetMock<IRepository<Product>>().Verify(x => x.Create(product), Times.Once);
            result.Should().BeOfType<BadRequestResult>();
        }

        [TestMethod]
        public async Task Post_Null_Should_Return_BadRequestResult()
        {
            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(false));

            //Testing post for null
            var result = await TestController.Post(null);
            result.Should().BeOfType<BadRequestResult>();
        }

        [TestMethod]
        public async Task Put_Null_Should_Return_BadRequestReslut()
        {
            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(false));

            var result = await TestController.Put(null);
            result.Should().BeOfType<BadRequestResult>();
        }

        [TestMethod]
        public async Task Put_Entity_Should_Return_OkReslut()
        {
            //creating product
            var product = new Product();

            //mocking
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(true));

            //testing
            var result = await TestController.Put(product);
            result.Should().BeOfType<OkResult>();
        }

        [TestMethod]
        public async Task Put_CommitSuccess_Should_Return_OkResult()
        {
            //Creating valid product
            var product = new Product();

            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(true));

            //Testing
            var result = await TestController.Post(product);
            result.Should().BeOfType<OkResult>();
        }

        [TestMethod]
        public async Task Put_CommitFailed_Should_Return_BadRequestResult()
        {
            //Creating valid product
            var product = new Product();

            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(false));

            //Testing
            var result = await TestController.Post(product);
            result.Should().BeOfType<BadRequestResult>();
        }

        [TestMethod]
        public async Task Delete_NotExistingProduct_Should_Return_BadRequestResult()
        {
            //Creating valid product
            Product product = new Product();

            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Get(product.Id)).Returns(product);
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(false));

            //Testing
            var result = await TestController.Delete(product.Id);
            result.Should().BeOfType<BadRequestResult>();
        }

        [TestMethod]
        public async Task Delete_ExistingProduct_Should_Return_OkResult()
        {
            //Creating valid product
            Product product = new Product();

            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Get(product.Id)).Returns(product);
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(true));

            //Testing
            var result = await TestController.Delete(product.Id);
            result.Should().BeOfType<OkResult>();
        }

        [TestMethod]
        public async Task Delete_CommitSuccess_Should_Return_OkResult()
        {
            //Creating valid product
            Product product = new Product(); ;

            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Get(product.Id)).Returns(product);
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(true));

            //Testing
            var result = await TestController.Delete(product.Id);
            result.Should().BeOfType<OkResult>();
        }

        [TestMethod]
        public async Task Delete_CommitFailed_Should_Return_BadRequestResult()
        {
            //Creating valid product
            var product = new Product();

            //Mocking
            GetMock<IRepository<Product>>().Setup(x => x.Get(product.Id)).Returns(product);
            GetMock<IRepository<Product>>().Setup(x => x.Commit()).Returns(Task.FromResult(false));

            //Testing
            var result = await TestController.Delete(product.Id);
            result.Should().BeOfType<BadRequestResult>();
        }
    }
}
